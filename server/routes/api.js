const debug_payment = require('debug')('app:payment');
const express = require('express');
const router = express.Router();
const srpago = require('../services/srpago/srpago');

router.get('/', (req, res) => {
  return res.send('home');
});

router.post('/checkout/payment', (req, res) => {
  const post = req.body;

  try {
    // Register a customer
    srpago.customer.create(post.customer)
      .then(customer => {
        // Link a card with a customer
        srpago.customer.addCard(customer.id, post.card.token)
          .then(card => {
            // Payment request
            srpago.payment.card({
              recurrent: card.token,
              payment: {
                reference: {
                  number: post.reference,
                  description: post.description,
                },
                total: {
                  amount: post.amount,
                  currency: post.currency,
                },
              },
              total: {
                amount: post.amount,
                currency: post.currency,
              },
            })
              .then(response => {
                res.json(response);
              })
              .catch(error => {
                debug_payment('An error occurred when associating a card with the customer.', error);

                res.send(error);
              });
          })
          .catch(error => {
            debug_payment('An error occurred when associating a card with the customer.', error);

            res.send(error);
          });
      })
      .catch(error => {
        debug_payment('An error occurred while creating a customer.', error);

        res.send(error);
      });
  } catch (error) {
    debug_payment('An unexpected error has occurred.', error);

    res.send(error);
  }
});

module.exports = router;
