const cors = require('cors');
const express = require('express');
const logger = require('morgan');
const path = require('path');

const router = express.Router();

const app = express();

app.use(logger('dev'));
app.use(cors({
  origin: '*',
}));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'));

app.use('/api', require('./routes/api'));
app.use('/', require('./routes/web'));

module.exports = app;
