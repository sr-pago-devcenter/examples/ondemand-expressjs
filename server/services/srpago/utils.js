const AesJS = require('aes-js');
const NodeRSA = require('node-rsa');
const fetch = require('node-fetch');
const C = require('./constants');

// Base64 implementation.
const b64 = {
  // Returns the base64 encoding of str.
  encode: str => {
    return Buffer.from(str).toString('base64');
  },

  // Returns the string represented by the base64 string str.
  decode: str => {
    return Buffer.from(str, 'base64').toString();
  },
};

// Generate random string
function RandomString(length, chars) {
  let mask = '';
  if (chars.indexOf('a') > -1) {
    mask += 'abcdefghijklmnopqrstuvwxyz';
  }

  if (chars.indexOf('A') > -1) {
    mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  }

  if (chars.indexOf('#') > -1) {
    mask += '0123456789';
  }

  if (chars.indexOf('!') > -1) {
    mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
  }

  let result = '';
  for (var i = length; i > 0; --i) {
    result += mask[Math.floor(Math.random() * mask.length)];
  }

  return result;
}

// ReplaceAccents
function ReplaceAccents(text) {
  const accentsMap = {
    a: 'á|à|ã|â|À|Á|Ã|Â',
    e: 'é|è|ê|É|È|Ê',
    i: 'í|ì|î|Í|Ì|Î',
    o: 'ó|ò|ô|õ|Ó|Ò|Ô|Õ',
    u: 'ú|ù|û|ü|Ú|Ù|Û|Ü',
    c: 'ç|Ç',
    n: 'ñ|Ñ',
  };

  return Object.keys(accentsMap).reduce(
    (acc, cur) => acc.replace(new RegExp(accentsMap[cur], 'g'), cur),
    text
  );
}

// AESEncrypt
function AESEncrypt(text, key) {
  const keyBytes = AesJS.utils.utf8.toBytes(key);

  text = ReplaceAccents(text);
  text = text + String.prototype.repeat.call(' ', 16 - (text.length % 16));
  const textBytes = AesJS.utils.utf8.toBytes(text);

  const aesEcb = new AesJS.ModeOfOperation.ecb(keyBytes);
  const encryptedBytes = aesEcb.encrypt(textBytes);

  return b64.encode(encryptedBytes);
}

// CipherPayload
exports.CipherPayload = function(payload) {
  const strPayload = JSON.stringify(payload);
  const salt = RandomString(32, '#aA');
  const payloadEncrypted = AESEncrypt(strPayload, salt);

  let encrypt = new NodeRSA(C.SRPAGO_ENCRYPT_PUBKEY);
  encrypt.setOptions({ encryptionScheme: 'pkcs1' });
  const saltEncrypted = encrypt.encrypt(salt, 'base64');

  return {
    key: saltEncrypted,
    data: payloadEncrypted,
  };
}

const isProduction = () => 'production' === process.env.SRPAGO_ENVIRONMENT;
exports.isProduction = isProduction;

// Requestor
exports.Requestor = function(uri, auth = false, options = {}) {
  let requestHeaders = {};

  if (auth) {
    requestHeaders = {
      ...{
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + b64.encode(`${process.env.SRPAGO_APPKEY}:${process.env.SRPAGO_SECRETKEY}`),
      },
      ...requestHeaders
    }
  }

  const requestOptions = {
    ...{
      headers: requestHeaders,
      redirect: 'follow'
    },
    ...options
  };

  const host = isProduction() ? C.SRPAGO_PROD_API : C.SRPAGO_SAND_API;
  console.log(`${host}${uri}`);

  return fetch(`${host}${uri}`, requestOptions);
}
