# SrPago

## OnDemand Examples

### API

```shell
# Install dependencies
$ npm i

# Define environment variables
$ cp .env.example .env
$ vim .env

# Start server
$ npm run dev
```

### Frontend

Open in browser http://0.0.0.0:3000
